<?php
  require "simplepie.inc";
  include "feeds.inc";

  function showitems($feed, $limit)
  {
    ?>
    <p>With <?php echo $feed->get_item_quantity() ?> items.</p>
    <?php
    foreach ($feed->get_items(0, $limit) as $item):
    ?>
      <div class="item">
        <p><a href="<?php echo $item->get_permalink(); ?>"><?php echo $item->get_title(); ?></a> <small>on <?php echo $item->get_date('j F Y | g:i a'); ?></small></p>
      </div>
    <?php
    endforeach;
    ?>
    <p><a href="<?php echo $feed->get_permalink(); ?>">permalink</a></p>
    <?php
  }
  
?>
<html>
  <head>
    <title>Calligra Quality Dashboard</title>
  </head>
  <body>
    <h1><a href="http://www.calligra-suite.org">Calligra</a> Quality Dashboard</a></h1>
    <h2>Tests results</h2>
    <h3>From cdash</h3>
    <?php
      showitems($cdash_feed, 5)
    ?>
    <h3>From teamcity</h3>
    <?php
      showitems($teamcity_feed, 5)
    ?>
    <h2>Bugs</h2>
    <h3>Release Blocker Bugs</h3>
    <?php
      showitems($opened_rb_bugs_feed, 0);
    ?>
    <h3>Crashes</h3>
    <?php
      showitems($opened_crash_bugs_feed, 0);
    ?>
    <h3>Critical</h3>
    <?php
      showitems($opened_critical_bugs_feed, 0);
    ?>
    <h3>Grave</h3>
    <?php
      showitems($opened_grave_bugs_feed, 0);
    ?>
    <h3>Major</h3>
    <?php
      showitems($opened_major_bugs_feed, 0);
    ?>
  </body>
</html>
